// Κάποια imports εδώ για το Test και τα assertions του junit

public class StringTransformTests {
    StringTransform stringTransform = new StringTransform;

    // Αυτό το τεστ και μόνο πιστεύω επαρκεί για το coverage, αλλά προφανώς δεν ελέγχει ακραίες περιπτώσεις
    @Test
    public void transfromStringWithANormalScenario() {
        String input = "This\\sis\\sa\\ssentence.";
        String expected = "This Is A Sentence.";
        String result = stringTransform.transformString(input);
        assertEquals(expected, result);
    }

    @Test
    public void transfromStringWithAnEmptyString() {
        String input = "";
        String expected = "";
        String result = stringTransform.transformString(input);
        assertEquals(expected, result);
    }

    @Test
    public void transfromStringWithOneWord() {
        String input = "word";
        String expected = "Word";
        String result = stringTransform.transformString(input);
        assertEquals(expected, result);
    }

    @Test
    public void transfromStringWithAllCaps() {
        String input = "ALL CAPS!";
        String expected = "All Caps!";
        String result = stringTransform.transformString(input);
        assertEquals(expected, result);
    }

    @Test
    public void transformStringWithNull() {
        String input = null;
        String expected = null; // Πρέπει να είναι null? Δεν έχει καθοριστεί, είναι ένα κενό στις προδιαγραφές εδώ. Το τεστ θα αποτύχει.
        String result = stringTransform.transformString(input);
        assertEquals(expected, result);
    }
}